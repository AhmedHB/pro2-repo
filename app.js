const http = require('http');
const fs = require('fs');


const hostname = '127.0.0.1';
const port = 3000;


const server = http.createServer( (req, res) => {

    fs.readFile('./info.txt', (err, data) => {
        res.setStatusCode = 200;
        res.setHeader('Content-Type', 'utf8', 'text/html');
        res.write(data);
        res.end();
    })   

})


server.listen(port, hostname, () => {
    console.log('Server is running on Port: ' + port + ' and Hostname: ' + hostname);
})
